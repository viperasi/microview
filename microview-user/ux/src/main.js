import './public-path'
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import routes from "./router";
import {AXIOS} from './components/http-common'

Vue.prototype.$axios = AXIOS
Vue.config.productionTip = false

let router = null
let instance = null

function render() {
    router = new VueRouter({
        base: window.__POWERED_BY_QIANKUN__ ? '/user/' : '/',
        mode: 'history',
        routes,
    });
    instance = new Vue({
        vuetify,
        router,
        render: h => h(App),
    }).$mount('#app');
}

if (!window.__POWERED_BY_QIANKUN__) {
    render()
}

export async function bootstrap() {
    // eslint-disable-next-line no-console
    console.log('vue app bootstraped')
}

export async function mount(props) {
    console.log('[vue] props from main framework', props);
    render(props);
}

export async function unmount() {
    instance.$destroy();
    instance.$el.innerHTML = '';
    instance = null;
    router = null;
}
