import Vue from 'vue';
import VueRouter from 'vue-router';
import Test from "../components/Test";
import Index from "../components/Index";
import Atest from "../components/Atest";

Vue.use(VueRouter)

const routes = [
    {
        path: '/test',
        name: 'Test',
        component: Test,
        meta: {
            requireLogin: false
        },
    },
    {
        path: '/',
        name: 'Index',
        component: Index,
        meta: {
            requireLogin: false
        }
    },
    {
        path: '/atest',
        name: 'Atest',
        component: Atest,
        meta: {
            requireLogin: false
        }
    }
]

export default routes
